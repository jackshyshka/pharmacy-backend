<?php

use yii\db\Migration;
require_once Yii::getAlias("@yii") . "/rbac/migrations/m140506_102106_rbac_init.php";
require_once Yii::getAlias("@yii") . "/rbac/migrations/m170907_052038_rbac_add_index_on_auth_assignment_user_id.php";


/**
 * Class m180118_160249_rbac_migrations
 */

class m180118_160249_rbac_migrations extends Migration
{
    public function up()
    {
        (new m140506_102106_rbac_init())->up();
        (new m170907_052038_rbac_add_index_on_auth_assignment_user_id())->up();
    }

    public function down()
    {
        (new m170907_052038_rbac_add_index_on_auth_assignment_user_id())->down();
        (new m140506_102106_rbac_init())->down();
    }

}
