<?php

use yii\db\Migration;

/**
 * Class m180117_170533_add_real_user_table
 */
class m180117_170533_add_real_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(\app\models\user\User::tableName(),[
            'id'=>$this->primaryKey()->unique(),
            'typeUser'=>$this->integer(1)->defaultValue(\app\models\user\User::TYPE_CLIENT),
            'email'=>$this->string(50)->unique()->notNull(),
            'needEmailMessages'=>$this->boolean()->null(),
            'phone'=>$this->string(15)->unique()->null(),
            'needPhoneMessages'=>$this->boolean()->null(),
            'region'=>$this->string(128)->null(),
            'city'=>$this->string(128)->null(),
            'street'=>$this->string(128)->null(),
            'houseNumber'=>$this->string(30)->null(),
            'firstName'=>$this->string(30)->null(),
            'middleName'=>$this->string(30)->null(),
            'lastName'=>$this->string(30)->null(),
            'accessToken'=>$this->string(64)->null(),
            'authKey'=>$this->string(64)->notNull(),
            'password'=>$this->string(64)->notNull(),

            'gender'=>$this->integer(1)->null(),
            'birthDay'=>$this->integer()->null(),

            'orgName'=>$this->string()->null(),
        ]);
        $this->insert(\app\models\user\User::tableName(),[
            'typeUser'=>\app\models\user\User::TYPE_ADMIN,
            'email'=>'jackshyshka@gmail.com',
            'needEmailMessages'=>true,
            'phone'=>'89782102415',
            'needPhoneMessages'=>'true',
            'region'=>'Sevastopol',
            'city'=>'Sevastopol',
            'street'=>'Nachimova',
            'houseNumber'=>'23',
            'firstName'=>'Yevheny',
            'middleName'=>'Aleksandrovich',
            'lastName'=>'Shyshka',
            'accessToken'=>null,
            'authKey'=>Yii::$app->security->generateRandomString(64),
            'password'=>Yii::$app->security->generatePasswordHash('111111'),

            'gender'=>'1',
            'birthDay'=>strtotime('29.12.1991'),

            'orgName'=>null,
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(\app\models\user\User::tableName());
    }
}
