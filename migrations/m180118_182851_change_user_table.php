<?php

use yii\db\Migration;

/**
 * Class m180118_182851_change_user_table
 */
class m180118_182851_change_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\user\User::tableName(),'state',$this->integer(1)->notNull()->defaultValue(\app\models\user\User::STATE_NEED_CONFIRM)." AFTER typeUser");

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(\app\models\user\User::tableName(),'state');
    }
}
