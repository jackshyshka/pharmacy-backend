<?php
/**
 * Created by PhpStorm.
 * User: svaig
 * Date: 18.01.18
 * Time: 22:04
 */

namespace app\modules\api\controllers;

use div\geoip\Geo;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\VarDumper;
use yii\rest\Controller;

class AuthController extends Controller
{

    public function init()
    {
        parent::init();
    }

    public function behaviors()
    {

        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                //HttpBasicAuth::className(),
                //HttpBearerAuth::className(),
                //QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function actionIndex(){
        /**
         * @var $geo Geo
         */
        $geo = Yii::$app->geo;
        $data = $geo->getData();
        return $data;


    }
}