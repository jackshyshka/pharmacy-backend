<?php

namespace app\modules\api\controllers;

use app\models\user\User;
use yii\base\InvalidConfigException;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public function init()
    {
        $this->modelClass = User::className();
        try {
            parent::init();
        } catch (InvalidConfigException $e) {
        }
    }
}
