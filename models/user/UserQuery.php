<?php
/**
 * Created by PhpStorm.
 * User: svaig
 * Date: 18.01.18
 * Time: 20:28
 */

namespace app\models\user;


use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{


    /**
     * @param null $db
     * @return array|User
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return User[]|null
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}