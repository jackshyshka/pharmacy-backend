<?php

namespace app\models\user;

use yii\db\ActiveRecord;

/**
 * Class User
 * @package app\models\user
 *
 * @property integer $id
 * @property integer $typeUser тип юзверя: админ, модератор, клиент, аптека, аптечная сеть
 * @property integer $state состояние учетной записи пользователя: требует активации, активна, забанена, удалена
 * @property string $email
 * @property bool $needEmailMessages
 * @property string $phone
 * @property bool $needPhoneMessages
 * @property string $region
 * @property string $city
 * @property string $street
 * @property string $houseNumber
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $accessToken
 * @property string $authKey
 * @property string $password
 *
 * для людишек:
 * @property integer $gender
 * @property integer $birthDay
 *
 * для аптек/сетей
 *
 * @property string $orgName
 *
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    const TYPE_ADMIN = 0;
    const TYPE_MODERATOR = 1;
    const TYPE_CLIENT = 2;
    const TYPE_PHARMACY = 3;
    const TYPE_PHARMACY_NETWORK = 4;

    const STATE_NEED_CONFIRM = 0;
    const STATE_ACTIVE = 1;
    const STATE_BANNED = 2;
    const STATE_DELETED = 3;

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;
    const GENDER_SOMETHING_ELSE = 3;


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::find()->where(['id'=>":id"],[":id"=>$id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where('accessToken = :token',[":token"=>$token])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * Проверка присланного пароля
     *
     * @param string $password присланный пароль
     * @return bool если пароль верен возвращает истину
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password,$this->password);
    }
}
