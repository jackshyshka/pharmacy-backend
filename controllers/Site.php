<?php
/**
 * Created by PhpStorm.
 * User: Svaig
 * Date: 21.01.2018
 * Time: 13:24
 */

namespace app\controllers;


use yii\web\Controller;
use yii\web\ErrorAction;

class Site extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\ErrorAction',
            ],
        ];
    }

}