<?php
/**
 * Created by PhpStorm.
 * User: Svaig
 * Date: 21.01.2018
 * Time: 13:28
 */

namespace app\components;


use yii\base\Action;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class ErrorAction extends \yii\rest\Action
{
    public function run()
    {
        // ошибка произошла вне API
        if (substr(\Yii::$app->request->pathInfo, 0, 4) != 'v1/') {
            return parent::run();
        }

        // подготовка данных

        $net = new ContentNegotiator([
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
            'languages' => [
                'en',
                'ru',
            ],
        ]);
        // подготавливаем \yii\web\Response
        $net->negotiate();

        // обработка ошибки внутри API
        $exceptionData = $this->currentExceptionData;
        $response = \Yii::$app->getResponse();
        $response->setStatusCode($exceptionData->code);
        $response->data = [
            'name' => $exceptionData->name,
            'message' => $exceptionData->message,
            'code' => $exceptionData->code,
        ];
        $response->send();
    }
}